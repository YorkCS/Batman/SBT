FROM amazoncorretto:11

ENV SBT_VERSION=1.4.6
ENV PATH="/usr/local/sbt/bin:${PATH}"

RUN yum update -y && yum install -y gzip tar wget && yum clean all
RUN export PATH="/usr/local/sbt/bin:$PATH" && mkdir -p "/usr/local/sbt" && wget -qO - "https://github.com/sbt/sbt/releases/download/v${SBT_VERSION}/sbt-${SBT_VERSION}.tgz" | tar xz -C /usr/local/sbt --strip-components=1
RUN mkdir /data && cd /data && sbt sbtVersion && cd ../ && rm -rf /data

ENTRYPOINT [sbt"]
